from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'users', views.ClientViewset, basename='client')
router.register(r'transactions', views.TransactionViewSet, basename='transaction')
router.register(r'tips', views.TipViewSet, basename='tip')
router.register(r'plans', views.PlanViewSet, basename='plan')
router.register(r'types', views.TransactionTypeViewSet, basename='type')
router.register(r'requests', views.RequestViewSet, basename='request')
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)), 
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('client/<str:email>/', views.profilePage),
    path('trans/<int:client_id>/', views.getTransByUser),
    path('statistics/<int:client_id>/', views.getRegression),
    path('trans/<int:client_id>/<str:client_date>/', views.getTransByUserDate),
    path('tr/<int:client_id>/<int:page>/', views.getTransPage),
    path('gps/', views.postRequest),
    path('pl/<int:client_id>/<int:month>/<int:year>/', views.getPlansByUserMonth)
]
