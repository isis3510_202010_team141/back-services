from rest_framework import serializers
from .models import Client, Plan, Tip, Transaction, PlannedTransaction, TransactionType, Request

""" Client"""
class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'email', 'phone', 'age', 'name']

class PlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = ['id', 'name', 'initialDate', 'incomeBudget', 'outcomeBudget', 'client']

class TipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tip
        fields = ['id', 'title', 'description']

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ['id', 'client','name', 'value', 'typeOf', 'date']

class PlannedTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlannedTransaction
        fields = ['id', 'name', 'value', 'plan', 'typeOf']

class TransactionTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TransactionType
        fields = ['number', 'name']

class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = ['name', 'amount']