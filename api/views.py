import numpy as np
import datetime
from django.shortcuts import render
from rest_framework import viewsets
from django.core.paginator import Paginator
from django.http.response import JsonResponse
from django.db.models import F
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
 
# Create your views here.

from .serializers import ClientSerializer, PlanSerializer, TransactionSerializer, TipSerializer, PlannedTransactionSerializer, TransactionTypeSerializer, RequestSerializer
from .models import Client, Plan, Tip, Transaction, PlannedTransaction, TransactionType, Request

def updateRequest(pName):
    (obj, created) = Request.objects.get_or_create(name=pName)
    obj.amount = F('amount') + 1
    obj.save()

class ClientViewset(viewsets.ModelViewSet):
    updateRequest("Client")
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

class PlanViewSet(viewsets.ModelViewSet):
    updateRequest("Plan")
    queryset = Plan.objects.all()
    serializer_class = PlanSerializer

class TransactionViewSet(viewsets.ModelViewSet):
    updateRequest("Transaction")
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

class PlannedTransactionViewSet(viewsets.ModelViewSet):
    updateRequest("PlannedTransaction")
    queryset = PlannedTransaction.objects.all()
    serializer_class = PlannedTransactionSerializer

class TipViewSet(viewsets.ModelViewSet):
    updateRequest("Tip")
    queryset = Tip.objects.all()
    serializer_class = TipSerializer

class TransactionTypeViewSet(viewsets.ModelViewSet):
    queryset = TransactionType.objects.all()
    serializer_class = TransactionTypeSerializer

class RequestViewSet(viewsets.ModelViewSet):
    queryset = Request.objects.all()
    serializer_class = RequestSerializer

@api_view(['GET', 'POST'])
def profilePage(request, email=None):
    if request.method == 'GET':
        updateRequest("GetUserByEmail")
        users = Client.objects.filter(email=email).first()
        serializer = ClientSerializer(users)
        return Response(serializer.data)

@api_view(['GET'])
def getTransByUser(request, client_id=None):
    if request.method == 'GET':
        updateRequest("GetTransactionsByUser")
        transactions = Transaction.objects.filter(client=client_id)
        serializer = TransactionSerializer(transactions, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def getRegression(request, client_id=None):
    if request.method == 'GET':
        updateRequest("GetRegression")
        transactions = Transaction.objects.filter(client=client_id).order_by('-date')
        x = []
        y = []
        i = 0
        for trans in transactions:
            if i == 8:
                break
            if trans.typeOf:
                i += 1
                x.append(i)
                y.append(float(trans.value))
        if i == 0:
            res = "No se han añadido transacciones en los ultimos 8 días"
        else:
            valX = np.array(x)
            valY = np.array(y)
            z = np.polyfit(valX, valY, 1)
            z = np.polyval(z, valX)
            res = str(z[-1])
        return JsonResponse({"value": res })


@api_view(['GET'])
def getNewRegression(request, client_id=None):
    if request.method == 'GET':
        updateRequest("GetRegression")
        transactions = Transaction.objects.filter(client=client_id).order_by('-date')
        plan = Plan.objects.filter(client=client_id).order_by('-initialDate').first()
        now = datetime.datetime.now()
        x = []
        y = []
        i = 0
        for trans in transactions:
            if trans.typeOf:
                i += 1
                if now.year == trans.date.year and now.month == trans.date.month:
                    x.append(trans.date.day)
                    y.append(float(trans.value))
        if i == 0:
            res = "No se han añadido transacciones en los ultimos 8 días"
        else:
            valX = np.array(x)
            valY = np.array(y)
            z = np.polyfit(valX, valY, 1)
            v = np.polyval(z, valX)
            res = str(v[-1])
            if(plan != None): #TO FINISH
                a = np.array([0, 1])
                b = np.array([float(plan.value), float(plan.value)])
                poly = np.polyfit(a, b, 1)
                #roots = np.roots(z-[float(plan.value), 0])
        return JsonResponse({"value": res })

@api_view(['GET'])
def getPlansByUserMonth(request, client_id=None, month=None, year=None):
    if request.method == 'GET':
        updateRequest("GetPlanByUserMonth")
        plan = Plan.objects.filter(initialDate__year__gte=year, initialDate__month__gte=month,
        initialDate__year__lte=year,initialDate__month__lte=month, client=client_id).order_by('-initialDate').first()
        serializer = PlanSerializer(plan)
        return Response(serializer.data)

@api_view(['GET'])
def getTransByUserDate(request, client_id=None, client_date=None):
    if request.method == 'GET':
        updateRequest("GetTransactionsByUserDate")
        date_time_obj = datetime.datetime.strptime(client_date, '%Y-%m-%d')
        transactions = Transaction.objects.filter(date__date=date_time_obj,client=client_id)
        serializer = TransactionSerializer(transactions, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def getTransPage(request, client_id=None, page=None):
    if request.method == 'GET':
        transactions = Transaction.objects.filter(client=client_id).order_by('-date')
        paginator = Paginator(transactions, 10)
        page_obj = paginator.get_page(page)
        serializer = TransactionSerializer(page_obj.object_list, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def postRequest(request):
    if request.method == 'GET':
        req, boolean = Request.objects.get_or_create(name="GPS")
        req.amount = F('amount') + 1
        req.save()
        req.refresh_from_db()
        serializer = RequestSerializer(req)
        return Response(serializer.data)