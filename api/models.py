from django.db import models
from django.utils.timezone import now

# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=60, null=True)
    email = models.CharField(max_length=60)
    phone = models.BigIntegerField(null=True)
    dateOfBirth = models.DateField(null=True)
    age = models.IntegerField(null=True)
    password = models.CharField(max_length=60, null=True)



class Plan(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length = 60, null=False, default="Monthly Budget")
    initialDate = models.DateTimeField(default=now)
    finalDate = models.DateTimeField(null=True)
    incomeBudget = models.DecimalField(decimal_places=3, max_digits=30, default=100000)
    outcomeBudget = models.DecimalField(decimal_places=3, max_digits=30, default=-1000)
    def __str__(self):
        return self.name


class Transaction(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=60)
    typeOf = models.BooleanField()
    value = models.DecimalField(decimal_places=3, max_digits=30)
    date = models.DateTimeField(default=now)
    def __str__(self):
        return self.name

class PlannedTransaction(models.Model):
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    name = models.CharField(max_length= 60)
    typeOf = models.BooleanField()
    value = models.DecimalField(decimal_places=3, max_digits=30)

class Tip(models.Model):
    title = models.CharField(max_length=60, null=False)
    description = models.CharField(max_length=600, null=False)
    content = models.CharField(max_length=600, null=False)
    def __str__(self):
        return self.title

class TransactionType(models.Model):
    number = models.IntegerField()
    name = models.CharField(max_length=20)

class Request(models.Model):
    name = models.CharField(max_length=60, null=False)
    amount = models.IntegerField(default=0)
