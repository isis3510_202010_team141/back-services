# Generated by Django 3.1.2 on 2020-12-11 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_request'),
    ]

    operations = [
        migrations.AddField(
            model_name='plan',
            name='value',
            field=models.DecimalField(decimal_places=3, default=100000, max_digits=30),
        ),
        migrations.AlterField(
            model_name='plan',
            name='name',
            field=models.CharField(default='Monthly Budget', max_length=60),
        ),
    ]
