#Back-Services

Repository that manages the back-end of the mobile app.

## How to Access

The Back service is currently deployed at: https://django-middle-finance.herokuapp.com/

## How to Run

1. Have a python virtual environment
2. Run `pip install -r requirements.txt`
3. Run `python manage.py runserver`